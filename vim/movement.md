# Show Off Movement
`` :help motion.txt ``


This portion is build more or less on the Thoughtbot video, "Mastering the Vim Language".


(Turn on partial commands first, to show what's going on!)
``
:set showcmd
``

This is not a comprehensive list, this is a place to start for those somewhat
frustrated with the current settings.

# Grammar of the Normal Mode
Most edits expressed to vim follow the Verb - Movement/Object.
## Verbs (modifiers)
``d``, ``p``, ``c[ia]``
## Movements/ Objects
``b``, ``e``, ``w``, ``s``, ``{``, ``}``
### Left-right motions
``h``, ``l``, ``f``, ``t``, ``0``, ``$``, ``^``, ``(``, ``)``
### Up-down motions
``j``, ``k``, ``gg``, ``G``

# Replacing (part 2)
If you noticed, ``c`` and ``s`` delete text before going into insert mode. You can repeat changes made to files by ``c`` or ``s`` with ``.``.
So, say you want to change a variable name in a file. So instead of doing:
	- Go to the variable name
	- Delete text
	- Go into insert mode
	- Rinse and repeat (delete, insert, delete, insert ...) on each occurrence of the name
You can instead do:
	- Go to the variable name
	- ``ciw``
	- Type the change
	- Rinse and repeat (``.``, ``.``, ``.``, ...) on each occurrence of the name

(You can even simplify the last step with repetitions... ``50.``)

# Other cool movement related things
## Screen stuff
H, M, L, ^L, ^F, ^B, ^D, ^B, z[ztb]
## Marks
m[a-Z], '[a-Z]
## MOUSE POINTERS??!?
`` :set mouse=a ``
