# Vim demonstration
*created by Benjamin Spriggs for the PDX-CS recitation sessions*.

## Disclaimers
1.  I don't know everything, google it if you don't know, or use
	+ `` :help <thing> ``
	+ Example: `` :help :x ``
2. Vim is a crappy WYSIWYG editor. Nano, pico does that better.  It is a
powerful text editor because you talk **directly** to the editor, and can do
interesting things with it. Typing and writing code can then become programmatic
and rather deterministic!

3. This presentation assumes some familiarity with
regexes, which some googling can give you some clear answers.

## Useful Resources
 - [Vim Adventures](http://vim-adventures.com/)
    - Quick game that teaches basic movements in vim, good to get basically
      acquainted to basic movement.
 - [Vim Commands](https://vimcommands.github.io/)
    - Basic vim commands. Good place to go to learn new things, after mastering
      what's in here.

 - [Vimcasts](vimcasts.org)
    - Some niche content, look at the beginning, good for medium-advanced usages
      of vim.

 - [Mastering the Vim Language](www.youtube.com/watch?v=wlR5gYd6um0)
    - Very good explanation of the idea that you talk to the editor, good
      introduction to the strengths of vim.

 - [Write code faster: expert-level
   vim](https://www.youtube.com/watch?v=SkdrYWhh-8s)
    - Ruby on Rails specific, but good for expanding a basic understanding of
      how vim works.

 - [From Vim Muggle to Wizard in 10 Easy
  Steps](https://www.youtube.com/watch?v=MquaityA1SM)
    - He's not kidding, he's a wizard, he gives a good lowdown on how vim came
      to be and how to use it good.


# Presentation
- introduction.md
- history.md
- movement.md
- completion.md
- dired.md
