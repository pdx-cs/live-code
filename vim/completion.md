# Completion
``:help ins-complete``
So VIm has been doing things for a few years now. There's another mode, called **completion mode**.

Do this in insert mode:
``^X``

You see the crazy set of letters at the bottom of the screen? In the status line?
Those are all of the completion modes that VIm can do.

## Modes
- ``^N`` - NEXT - Complete from similar instances of the text after the cursor in the file
- ``^P`` - PREVIOUS - Complete from similar instances before the cursor in the file
- ``^L`` - LINE - Complete from similar lines in the file
- ``^F`` - FILE - Complete from filenames in the current directory
- ``^K`` - DICTIONARY - Complete from similar words in a user-defined dictionary ([More details](http://vim.wikia.com/wiki/Dictionary_completions))


### Omni complete
``:help omni``
So vim has [omni](http://usevim.com/2012/07/06/vim101-completion/) complete. It's exactly as omni as you think it is.

Load up a file in ``example/``. Enable omni completion:
```
filetype plugin on
:set omnifunc=syntaxcomplete#Complete
```

Now use ``^X^O``. Try it out, experiment with these completions!
