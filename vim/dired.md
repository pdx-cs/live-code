# Directory Editing
This is a pretty interesting thing, ever tried opening a directory before?

``` :tabe example ```

You're presented with a directory editor, or you're put into "dired" mode.  You
can open, delete, etc in this mode (which is pretty sick imo):
 - ``gf`` 'goes' to the current file under the cursor
 - ``o`` opens the current file as a new window/ pane.

You can do all the same things with search and replace on this buffer. Try it out!
