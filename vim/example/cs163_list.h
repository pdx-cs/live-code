//list.h
#include <iostream>
#include <cstring>
#include <cctype>
#include <cstdlib>


//For practice we will be using a data structure of integers
//This is set up for a doubly linked list
struct node
{
    int data;
    node * previous;
    node * next;
};

//These functions have already been implemented
void build(node * & head);  //supplied
void display(node * head);  //supplied
void destroy(node * &head); //supplied

