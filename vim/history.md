# History of the VIm editor (Brief)

"Vim is based on 'verb modifier object' sentences, turned into acronyms ... "
- Yan Pritzker [Learn to speak vim verbs nouns and modifiers] (https://yanpritzker.com/learn-to-speak-vim-verbs-nouns-and-modifiers-d7bfed1f6b2d)

# History 
"Vim" is from Vi improved, a line editor developed by Bill Joy in
1976, which was based on a line editor called ex, written in tandem with Chuck
Haley
[citation](https://web.archive.org/web/20120210184000/http://web.cecs.pdx.edu/~kirkenda/joy84.html).
Generally, vi was used to do specific edits (delete that thing, go here, then
do this) and to be able to do them repetitively over a slow connection that
  made quick iteration difficult.
tl;dr VIm comes from a rather abstruse history.
