# Introduction
VIm is a moded editor(``:help Normal-mode``) The default mode is "**normal mode**", where commands to the
editor are given.

```
vim
```

Do this, and you're in normal mode. Commands are prefixed with ``:``.
Like: `` :wa `` Will save all your open buffers.
`` :q `` Will close the current buffer, etc.

If at any point you need to **undo a change**, you can use ``u``!
Most commands can be repeated by prepending or appending a number to them. Try it out!

## Insert Mode
Insert mode is the notepad mode, text is directly entered into the current file,
       or buffer. There are two submodes:
       - ``i`` will take you into **insert** mode,
       - ``a`` into **append** mode.

       Uppercase versions of both  ``I``, ``A`` will go into that mode at the beginning of the file.
### Interesting operations to continue looking at
J, S, D, C

## Searching
       You can search text by starting in normal mode, then doing one of the following:
       - ``/text`` finds all text that matches in the file, looking forward from the cursor
       - ``?text`` same as above, only starting backwards

Both of these can be appended with ``/<count>`` to go <count> lines below the match.

## Replacing
Replacing uses similar syntax to the above (in normal mode):
	- ``:s/text`` Deletes all occurences of text on the line
	- ``:s/text/replace`` Deletes and replaces text with replace
	- ``:s/text/replace/g`` Deletes and replaces, across the line

We'll be going into another way of more generally replacing text later.

## Selecting
There is another mode, called **visual mode**. It allows for selecting ranges of text (such as copying a few lines of code to paste somewhere else).

You can enter visual mode a number of ways:
	- ``v`` in normal mode - goes into visual select mode
	- ``V`` in normal mode - goes into visual *line* select mode
	- ``<Ctrl>V`` in normal mode - goes into visual *block* select mode

Experiment with these modes on (lorem.txt).
