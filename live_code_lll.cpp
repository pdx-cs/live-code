#include "list.h"

int insert_before(node *& head){

    if (!head)
        return 0;

    int retval = 0;

    if (head->data % 2 == 0)
        ++retval;

    retval += insert_before(head->next);

    if (head->data % 3 == 0
            || head->data % 5 == 0) {
        node * temp = new node;
        temp->data = 1337;
        temp->next = head;
        head = temp;
    }

    return retval;
}
