#include "list.h"
#include "cll.h"

int insert_before(node*& head)
{
    int ret = 0;

    if(!head)
        return 0;

    ret = insert_before(head->next);

    if(!(head->data%3) || !(head->data%5))
    {
        node* temp = head;
        head = new node;
        head->data = 9999;
        head->next = temp;
    }

    if(!(head->data%2))
        ++ret;

    return ret;
}

int remove_special_case(node*& head)
{
    if(!head)
        return 0;

    int largest = head->data;
    int comp = head->data;
    return real_remove_special_case(head, comp, largest);
}

int real_remove_special_case(node*& head, const int comp, int& largest)
{
    int ret = 0;

    if(!head)
        return 0;

    if(head->data > largest)
        largest = head->data;

    ret = real_remove_special_case(head->next, comp, largest);

    if(head->data == largest)
    {
        node* temp = head;
        head = head->next;
        delete temp;
    }

    return (head->data == comp ? ++ret : ret);
    // if (head -> data == comp)
    //     ++ret;

    // return ret;
}

int copy_list(node*& dest, const node* source)
{
    if (!source)
    {
        dest = NULL;
        return 0;
    }

    dest = new node;
    dest -> data = source -> data;
    return 1 + copy_list(dest->next, source->next);
}

int copy_cll(node*& dest, const node* source)
{
    if (!source)
        return 0;
    return copy_cll( ... );
}

int copy_cll(node*& dest_current, node*& dest_head,
        const node* source_current, const node* source_rear)
{
    if (source_current == source_rear)
    {
        dest_current = new node;
        dest_current -> data = source_current -> data;
        dest_current -> next = dest_head;
        return 1;
    }

    dest_current = new node;
    dest_current -> data = source_current -> data;
    return copy_cll( hey g++ just put the right stuff here thnx ;) );
}


