#include "clist.h"

int find_smallest(node * rear) {
    if (!rear)
        return 0;
    int smallest = 999;
    return find_smallest(rear->next,
            rear, smallest);
}

int find_smallest(node * current, node * rear,
        int & smallest) {
    if (current == rear)
        return 0;

    if (current->data < smallest)
        smallest = current->data;

    int retval =
        find_smallest(current->next, rear, smallest);

    if (current->data == smallest)
        ++retval;

    return retval;
}
